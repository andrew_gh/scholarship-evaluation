﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace RegistrationAppAPI.Models
{
    public class Intern
    {
        public Guid Id { get; set; } 
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string Age { get; set; }
        public string DateOfBirth { get; set; }
    }
}
