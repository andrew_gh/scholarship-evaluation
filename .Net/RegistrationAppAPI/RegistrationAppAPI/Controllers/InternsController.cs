﻿using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using RegistrationAppAPI.Models;
using RegistrationAppAPI.Services;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace RegistrationAppAPI.Controllers
{
    [Route("[controller]")]
    [ApiController]
    public class InternsController : ControllerBase
    {
        private IInternCollectionService _internCollectionService;

        public InternsController(IInternCollectionService internCollectionService)
        {
            _internCollectionService = internCollectionService ?? throw new ArgumentNullException(nameof(internCollectionService));
        }

        [HttpPost]
        public async Task<IActionResult> CreateInternAsync(Intern intern)
        {
            if (intern == null)
                return BadRequest("Intern cannot be null");
            await _internCollectionService.Create(intern);
            return CreatedAtRoute("GetIntern", new { id = intern.Id }, intern);
        }

        [HttpGet]
        public async Task<IActionResult> GetAllInternsAsync()
        {
            List<Intern> interns = await _internCollectionService.GetAll();
            return Ok(interns);
        }

        [HttpGet("{id}", Name = "GetIntern")]
        public async Task<IActionResult> GetInternAsync(Guid id)
        {
            var intern = await _internCollectionService.Get(id);
            if (intern == null)
                return NotFound("There is no intern with the given Id");
            return Ok(intern);
        }

        [HttpDelete("{id}")]
        public async Task<IActionResult> DeleteInternAsync(Guid id)
        {
            bool ok = await _internCollectionService.Delete(id);
            if (!ok)
            {
                return NotFound("Intern not found");
            }
            return Ok("Intern deleted");
        }

        [HttpPut("{id}")]
        public async Task<IActionResult> UpdateInternAsync(Guid id, Intern intern)
        {
            if (intern == null)
                return BadRequest("intern can not be null");
            if (await _internCollectionService.Update(id, intern))
                return Ok("Update successful");
            return NotFound("There is no intern with the given Id");
        }
    }
}
