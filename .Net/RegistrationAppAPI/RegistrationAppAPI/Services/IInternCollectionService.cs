﻿using RegistrationAppAPI.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace RegistrationAppAPI.Services
{
    public interface IInternCollectionService : ICollectionService<Intern>
    {

    }
}
