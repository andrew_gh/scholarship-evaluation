import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { Observable } from 'rxjs';
import { Intern } from '../interfaces/intern';
import { InternService } from '../services/intern.service';

@Component({
  selector: 'app-add-intern',
  templateUrl: './add-intern.component.html',
  styleUrls: ['./add-intern.component.scss']
})
export class AddInternComponent implements OnInit {

  internFirstName: string | undefined;
  internLastName: string | undefined;
  internAge: string | undefined;
  internDateOfBirth: string | undefined;

  parameter:string;
  interns:Intern[];
  intern:Intern;
  intern$:Observable<Intern>;
  internHtml: Intern;

  constructor(private internService: InternService,private _router: Router, private _activatedRoute: ActivatedRoute) { }
  ngAfterViewInit(): void {
    this.internFirstName=this.intern.firstName;
    this.internLastName=this.intern.lastName;
    this.internAge=this.intern.age;
    this.internDateOfBirth=this.intern.dateOfBirth
  }
  ngOnInit(): void {
    
    this._activatedRoute.params.subscribe(parameter => {
      this.parameter=parameter["id"];
      if(this.parameter != "add"){
        this.intern$=this.internService.getIntern(this.parameter);
      }
    })
  }

  editNote(n:Intern){
    const intern: Intern = {
      id: n.id,
      firstName: n.firstName,
      lastName: n.lastName,
      age: n.age,
      dateOfBirth: n.dateOfBirth
    }
    this.internService.editIntern(intern).subscribe();
    this.internService.getInterns().subscribe((interns) => {this.interns=interns})
    this._router.navigate(['']);
  }

  addNote(){
    const intern: Intern = {
      firstName: this.internFirstName,
      lastName: this.internLastName,
      age: this.internAge,
      dateOfBirth: this.internDateOfBirth
    }
    this.internService.addIntern(intern).subscribe();
    this.interns.push(intern);
    this._router.navigate(['']);
  }
}
