import { Component,OnInit} from '@angular/core';
import { Router } from '@angular/router';
import { Intern } from '../interfaces/intern';
import { InternService } from '../services/intern.service';

@Component({
  selector: 'app-intern',
  templateUrl: './intern.component.html',
  styleUrls: ['./intern.component.scss']
})
export class InternComponent implements OnInit{

  interns: Intern[];

  constructor(private service: InternService, private _router: Router) { }

  editIntern(id:string){
    this._router.navigate([`intern-view/${id}`]);
  }

  deleteIntern(idToBeDeleted:string){
    this.service.deleteIntern(idToBeDeleted).subscribe();
    this.interns=this.interns.filter(n => n.id!==idToBeDeleted);
  }

  ngOnInit(): void {
    this.service.getInterns().subscribe((interns:Intern[]) => {this.interns=interns} );
  }

}
