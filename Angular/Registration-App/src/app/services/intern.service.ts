import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { HttpClient, HttpHeaders } from '@angular/common/http'
import { Intern } from '../interfaces/intern';

@Injectable({
  providedIn: 'root'
})
export class InternService {

  readonly baseUrl= "https://localhost:44326";
  readonly httpOptions = {
    headers: new HttpHeaders({
      'Content-Type':  'application/json',
    })
  };


  constructor(private httpClient: HttpClient) { }

  getIntern(id: string):Observable<Intern> {
    const url=this.baseUrl+ `/Interns/`+id;
    return this.httpClient.get<Intern>(url, this.httpOptions);
  }

  getInterns():Observable<Intern[]> {
    return this.httpClient.get<Intern[]>(this.baseUrl + `/Interns`, this.httpOptions);
  }

  addIntern(intern:Intern){
    console.log(intern);
    return this.httpClient.post(this.baseUrl + `/Interns`,intern ,this.httpOptions);
  }

  editIntern(intern:Intern){
    return this.httpClient.put(this.baseUrl + `/Interns/`+ intern.id ,intern,this.httpOptions);
  }

  deleteIntern(id: string){
    return this.httpClient.delete(this.baseUrl +`/Interns/${id}`,this.httpOptions);
  }
}
